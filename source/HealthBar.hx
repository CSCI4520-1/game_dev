package;

import flash.display.Sprite;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;

/**
 * ...
 * @author ...
 */
class HealthBar extends FlxSpriteGroup 
{
	var health_array = new Array();
	var icon_width:Int; // dimensions for the health icon sprite
	var icon_height:Int;
	var distance_from_left:Int = 20;
	var distance_from_top:Int = 80;
	var health_text:FlxSprite;	
	public function new(?X:Float=0, ?Y:Float=0,  ?width:Int=0, ?height:Int=0) 
	{
		super(X, Y);
		icon_width = width;
		icon_height = height;
		for (i in 0...5) {
			addHealthIcon();
		}
		
		health_text = new FlxSprite(distance_from_left, distance_from_top - 30);
		health_text.loadGraphic("assets/images/health_text.png");
		add(health_text);
	}
	
	public function addHealthIcon():Void{
		if (health_array.length == 0) { // we are going from 0 to one, place icon at absolute location at top right corner
			var pie_health = new FlxSprite(distance_from_left, distance_from_top);
			pie_health.loadGraphic("assets/images/health_pie.png");
			add(pie_health);
			health_array.push(pie_health); // push the icon to the end of the array
		}
		else { // we already have a health icon there, place it a certain distance away from the last one
			var pie_health = new FlxSprite(distance_from_left + (icon_width * 2 * health_array.length), distance_from_top);
			pie_health.loadGraphic("assets/images/health_pie.png");
			add(pie_health);
			health_array.push(pie_health); // push the icon to the end of the array
		}
	}
	
	public function deleteHealthIcon():Void{
		if (health_array.length > 0) {
			(health_array.pop()).destroy(); // pops the last health icon from the array and also destroys it
		}	
	}
	
	public function checkHealth():Int{
		return health_array.length;
	}
	
}