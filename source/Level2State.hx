package;

import flixel.FlxState;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import post_level_2.PostLevel2State1;

class Level2State extends LevelState
{
	
	override public function create():Void
	{
		level = 2;
		super.create();
		waveNo = 1;
		_player.setSpecialAvailable(this);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (levelThree == true){
			advanceToLevel3();
		}
	}
	
	private function advanceToLevel3():Void
	{
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function()
		{
			FlxG.switchState(new PostLevel2State1());
		});
	}
}
