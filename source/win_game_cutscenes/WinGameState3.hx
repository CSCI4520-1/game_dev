package win_game_cutscenes;

import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxTimer;
import win_game_cutscenes.WinGameState4;

class WinGameState3 extends FlxState
{
	private var _background:FlxSprite;
	private var cutsceneTimer:FlxTimer = new FlxTimer();
	
	override public function create():Void
	{
		FlxG.camera.fade(FlxColor.BLACK, .33, true);
		_background = new FlxSprite();
		_background.loadGraphic("assets/images/win game text/win game text 3.png");
		add(_background);
		super.create();
		cutsceneTimer.start(10, next, 1);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	private function next(Timer:FlxTimer):Void
	{
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function() // longer fade to black when starting game
		{
			FlxG.switchState(new WinGameState4());
		});
	}
}
