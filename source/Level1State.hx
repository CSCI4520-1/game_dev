package;

import flixel.FlxState;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import post_level_1.PostLevel1State1;

class Level1State extends LevelState
{
	
	override public function create():Void
	{
		level = 1;
		super.create();
		waveNo = 1;
		
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (levelTwo == true){
			advanceToLevel2();
		}
	}
	
	private function advanceToLevel2():Void
	{
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function()
		{
			FlxG.switchState(new PostLevel1State1());
		});
	}
}
