package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.util.FlxTimer;
import flixel.system.FlxSound;
import flixel.effects.FlxFlicker;
import flixel.tweens.FlxTween;
import lime.math.Vector2;

/**
 * ...
 * @author Eryka Greaves
 * @author Matt Castellana
 * @author Noah Zucker
 */
class Player extends FlxSprite
{
	var speed:Float = 100;
	var _vAngle:Float = 0;
	var _rot:Float = 0;
	
	// Keys
	var _up:Bool = false;
	var _down:Bool = false;
	var _left:Bool = false;
	var _right:Bool = false;
	
	// Attack Vars
	var _leftShoot:Bool = false;
	var _rightShoot:Bool = false;
	var _upShoot:Bool = false;
	var _downShoot:Bool = false;
	var _attackTimer:FlxTimer = new FlxTimer();
	var _attackAllowed:Bool = true;
	var _throwTimer:FlxTimer = new FlxTimer();
	var _justPressedDelay:FlxTimer = new FlxTimer(); //delays a very small amount so diagonal input can be read as a whole
	var _detectDiagonalDelay:Bool;

	var _animationPlaying:Bool = false;
	var _typeOfAttack:String = "normal";
	var _specialAttack:Bool = false;
	var _scapegoatCount:Int = 3; // number of scapegoat USBs available
	var _specialAvailable:Bool = false;
	private var _specialActivatedOverlay:FlxSprite;
	var _playerSprite:FlxSprite;
	// Defense Vars
	var _invincible:Bool = false;
	var _invincibilityTimer:FlxTimer = new FlxTimer();
	
	// Dimensions
	var _width:Int = 66;
	var _height:Int = 230;
	
	var _usbs:FlxTypedGroup<USB>;
	var _usbInfo:USBInfo;
	
	private var jumped:Bool = false;
	
	///////////
	//Set up sounds
	//private var _jumpSnd:FlxSound;
	private var _jumpSound:FlxSound;
	private var _throwSound:FlxSound;
	//////////
	
	public function new(?X:Float=0, ?Y:Float=0, ?usbGroup:FlxTypedGroup<USB>, ?level:FlxState) {
		super(X,Y);
		_playerSprite=loadGraphic("assets/images/Granny_all_action.png", true, 256, 256);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		this.height = 250;
		this.width = 150;
		animation.add("walk", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 10, false);
		animation.add("idle", [13, 14, 15, 16, 17, 18, 19], 10, false);
		animation.add("throw", [20, 21, 22, 23, 24], 25, false);
		animation.add("in_air_throw", [22], 0, false);

		animation.add("jump_start", [27,28], 10, false);
		animation.add("jump_end",  [29], 10, false);
		this.width = 66;
		this.height = 210;
		this.offset = new FlxPoint(100,25);
		///////////
		//Set up sounds
		//_jumpSnd = FlxG.sound.load(AssetPaths.jump_sound__wav);
		_jumpSound = FlxG.sound.load(AssetPaths.jumpSound__wav);
		_throwSound = FlxG.sound.load(AssetPaths.throwSound__wav);
		///////////
		
		// special activated background
		_specialActivatedOverlay = new FlxSprite();
		_specialActivatedOverlay.loadGraphic(AssetPaths.usb_fullscreen__png);
		_specialActivatedOverlay.screenCenter();
		level.add(_specialActivatedOverlay);
		FlxTween.tween(_specialActivatedOverlay, {alpha: 0.5}, 1, {type: FlxTween.PINGPONG}); // add pulsing effect to special overlay
		_specialActivatedOverlay.visible = false; // default to off
		
		
		drag.x = drag.y = 1300;
		_usbs = usbGroup;
		_attackTimer.start(0.35, allowAttack, 0); // start the timer that controls how fast USBs are shot
	}
	override public function update(elapsed:Float):Void {
		super.update(elapsed);
		poll();
		movement();
		attack(elapsed);
		if (FlxG.keys.anyJustPressed([G])){ //god mode
			_invincible = !_invincible;
		}
	}
	
	public function setSpecialAvailable(?level:FlxState) {
		_usbInfo = new USBInfo(this);
		level.add(_usbInfo);
		_specialAvailable = true;
	}
	
	// if granny gets hit, set invincibility to true, but also a timer to set it back to false
	public function setInvincibilityOnCollision() {
		_invincible = true;
		_invincibilityTimer.start(3, disableInvincibility, 1);
		FlxFlicker.flicker(this, 3);
	}
	
	public function disableInvincibility(Timer:FlxTimer) {
		_invincible = false;
		this.width = 66;
		this.height = 230;
	}
	
	public function getPlayerHeight():Int{
		return _height;
	}
	
	public function allowAttack(Timer:FlxTimer):Void{
		_attackAllowed = true;
	}
	
	private function toggleWeaponType() {
		if (_typeOfAttack == "normal") {
			_typeOfAttack = "scapegoat";
		}
		else {
			_typeOfAttack = "normal";
		}
	}
		
	function poll():Void {
		// Movement
		_up = FlxG.keys.anyJustPressed([W]);
		_left = FlxG.keys.anyPressed([A]);
		_right = FlxG.keys.anyPressed([D]);
		
		if (FlxG.keys.anyJustPressed([W])&&!jumped){
			//ff_playerSprite.scale = new FlxPoint(_playerSprite.scale.x * 1.18, _playerSprite.scale.y * 1.18);
		}
		// Attack
		
			_leftShoot = FlxG.keys.anyPressed([LEFT]);
		
			_rightShoot = FlxG.keys.anyPressed([RIGHT]);
			
			_upShoot = FlxG.keys.anyPressed([UP]);
		
			_downShoot = FlxG.keys.anyPressed([DOWN]);
		
			
			//delays a very small amount so diagonal input can be read as a whole
			if (FlxG.keys.anyJustPressed([LEFT])||FlxG.keys.anyJustPressed([RIGHT])||FlxG.keys.anyJustPressed([UP])||FlxG.keys.anyJustPressed([DOWN])) {
				_justPressedDelay.start(0.05, diagonalDelayEnable, 1); 
			}
			if(!_leftShoot&&!_rightShoot&&!_downShoot&&!_upShoot){
				if (FlxG.keys.anyPressed([LEFT])||FlxG.keys.anyJustReleased([RIGHT])||FlxG.keys.anyJustReleased([UP])||FlxG.keys.anyJustReleased([DOWN])) {
					_detectDiagonalDelay = false;
				}
			}
			
			
		///////////////////
		//Play sounds 
		
				
		///////////////////
		
		// Pressing Space activates special attack. Special can be deactivated by pressing space again or by using the special.
		if (_specialAvailable) {
			if (!_specialAttack) { // special attack has not been activated
				_specialAttack = FlxG.keys.anyJustPressed([SPACE]); // activate special if key pressed and lock selection for now
				if(FlxG.keys.anyJustPressed([SPACE])) {
					_typeOfAttack = "scapegoat";
					_specialActivatedOverlay.visible = true;
					_usbInfo.readySpecial();
				}
			}
			else { // special has been selected
				if (FlxG.keys.anyJustPressed([SPACE])) { // player wants to deactivate special
				_specialAttack = false;
				_specialActivatedOverlay.visible = false;
				_typeOfAttack = "normal";
				_usbInfo.unreadySpecial();
				}
			}
		}
		
	}
	
	//function resetPolling():Void {
	//	_leftShoot = false;
	//	_rightShoot = false;
	//	_upShoot = false;
	//}
	function throwAni(Timer:FlxTimer):Void {
		_animationPlaying = false;
	}
	function diagonalDelayEnable(Timer:FlxTimer):Void {
		_detectDiagonalDelay = true; //delays a very small amount so diagonal input can be read as a whole
	}
	function attack(elapsed:Float):Void {
		if (!_attackAllowed||!_detectDiagonalDelay) { // shoot delay
			return;
		}

		
		if(_throwTimer.progress==0) {//if timer for shooting has run out


		if (_leftShoot || _rightShoot || _upShoot || _downShoot) {		
			//Play Sound
			
			//Animation
			var _usbCreated:Bool=false;


			if (_leftShoot && _rightShoot || _upShoot && _downShoot) { // can't shoot in both directions, granny!
				_leftShoot = false;
				_rightShoot = false;
				_upShoot = false;
				_downShoot = false;
				return;
			}
			
			else if (_leftShoot) {
				facing = FlxObject.LEFT;
				if (_upShoot) { // diagonal to left
					_usbs.add(new USB(this.x, this.y, "up-left", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else if (_downShoot && this.velocity.y != 0) {
					_usbs.add(new USB(this.x, this.y, "down-left", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else { // horizontal left
					_usbs.add(new USB(this.x, this.y, "left", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				if (_specialAttack) {
					_specialAttack = false; // always deactivate special after shooting
					_specialAvailable = false;
					_usbInfo.useSpecial();
					_typeOfAttack = "normal";
					_usbCreated = true;
					_attackAllowed = false;
					_specialAttack = false;
					_specialActivatedOverlay.visible = false;
					_typeOfAttack = "normal";
					_usbInfo.unreadySpecial();
				}
			}
			else if (_rightShoot) {
				facing = FlxObject.RIGHT;
				if (_upShoot) { // diagonal to right
					_usbs.add(new USB(this.x, this.y, "up-right", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else if (_downShoot && this.velocity.y != 0) {
					_usbs.add(new USB(this.x, this.y, "down-right", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else { // horizontal left
					_usbs.add(new USB(this.x, this.y, "right", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				if (_specialAttack) {
					_specialAttack = false; // always deactivate special after shooting
					_specialAvailable = false;
					_usbInfo.useSpecial();
					_typeOfAttack = "normal";
					_usbCreated = true;
					_attackAllowed = false;
					_specialAttack = false;
					_specialActivatedOverlay.visible = false;
					_typeOfAttack = "normal";
					_usbInfo.unreadySpecial();
				}
			}
			else if (_upShoot) {
				if (_leftShoot) { // diagonal to left
					_usbs.add(new USB(this.x, this.y, "up-left", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else if (_rightShoot) { // diagonal to right
					_usbs.add(new USB(this.x, this.y, "up-right", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				else { // up
					_usbs.add(new USB(this.x, this.y, "up", _typeOfAttack));
					_usbCreated = true;
					_attackAllowed = false;
				}
				if (_specialAttack) {
					_specialAvailable = false;
					_specialAttack = false; // always deactivate special after shooting
					_usbInfo.useSpecial();
					_typeOfAttack = "normal";
					_usbCreated = true;
					_attackAllowed = false;
					_specialAttack = false;
					_specialActivatedOverlay.visible = false;
					_typeOfAttack = "normal";
					_usbInfo.unreadySpecial();
				}
			}
			else if (_downShoot && this.velocity.y != 0&& !_upShoot && !_leftShoot && !_rightShoot) {
				_usbs.add(new USB(this.x, this.y, "down", _typeOfAttack));
				_usbCreated = true;
			}
			_leftShoot = false;
			_rightShoot = false;
			_upShoot = false;
			_downShoot = false;
			if (_usbCreated) {
				if (_typeOfAttack == "scapegoat") {
					_specialAttack = false;
					_specialActivatedOverlay.visible = false;
					_typeOfAttack = "normal";
					_usbInfo.unreadySpecial();
				}
				_throwSound.play(true);
				_animationPlaying = true;
				_attackAllowed = false;
				_throwTimer.start(0.35, throwAni, 1);
				if (_usbCreated&&jumped) {
					animation.play("in_air_throw");
					
					_playerSprite.scale = new FlxPoint(1,1);
				}
				else {
					animation.play("throw");
				}
			}


			
		}
		
	}
	}
	
	function movement():Void {
		if (_up && !jumped) {

			//Play sound
			_jumpSound.play(true);

			jumped = true;
			// initial velocity.y is negative
			// positive acceleration
			// stop at "collision"
			velocity.y = -1000;
			acceleration.y = 1400;
			speed = 250;
		}
		if (jumped) {
			// Do mid-air logic here
			if (_left) {
				velocity.x = -500;
				if(!_leftShoot&&!_rightShoot&&!_downShoot&&!_upShoot) {
				facing = FlxObject.LEFT;
				}
			} else if (_right) {
				velocity.x = 500;
				if(!_leftShoot&&!_rightShoot&&!_downShoot&&!_upShoot) {
				facing = FlxObject.RIGHT;
				}
			}
			if (!_leftShoot && !_rightShoot && !_downShoot && !_upShoot && this.velocity.y < 700) {
			
			_playerSprite.scale = new FlxPoint(1.18,1.18);
			animation.play("jump_end");
			}
		} else {
			// Do floor logic
			if (_right || _left) {
				speed = 500;				
				velocity.set(speed,velocity.y); // Length and angle of velocity
				velocity.rotate(new FlxPoint(0, 0), _rot);
				if (_left) {
					_rot = 180;
				} else if (_right) {
					_rot = 0;
				}
				if(!_leftShoot&&!_rightShoot&&!_upShoot&&!_downShoot) {
				if (_left) {
					//_rot = 180;
					facing = FlxObject.LEFT;
				} else if (_right) {
					//_rot = 0;
					facing = FlxObject.RIGHT;
				}

				animation.play("walk");
				}
			} else {
				if (!_animationPlaying) {
					animation.play("idle");
				} 
			}
		}
		//making kinder hitbox when in air
		if (this.velocity.y <-800) {
			
			this.height = 175;
		}
		if (this.velocity.y > 800) {
			this.height = 210;
		}
		
		//tighter controls, stops granny from sliding when velocity is low enough and keys aren't pressed
		if (!_left && !_right && Math.abs(this.velocity.x) < 250){
			this.velocity.x = 0;
		}
		if (FlxG.keys.anyJustPressed([S])&&velocity.y<400&&velocity.y>-400){
			this.velocity.y = 700;
		}

		
		
	}
	
	public function isInvincible():Bool {
		return _invincible;
	}
	
	public function land():Void {
		velocity.y = 0;
		acceleration.y = 0;
		jumped = false;
		_playerSprite.scale = new FlxPoint(1, 1);
	}
	public function jumpStatus():Bool {
		if (jumped) {
			return true;
		} else {
			return false;
		}
	}
	public function enableSpecialAvailable()
	{
		_specialAvailable = true;
	}
	// TODO
	public function playDeathAnimation():Void{
		
	}
}
