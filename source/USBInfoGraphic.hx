package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.FlxG;
import flixel.effects.FlxFlicker;
import flixel.util.FlxSpriteUtil;
import flixel.tweens.FlxTween;
/**
 * ...
 * @author ...
 */
class USBInfoGraphic extends FlxSprite 
{
	var flickering:FlxTween;
	
	public function new(?X:Float=0, ?Y:Float=0, ?type:String) 
	{
		super(X, Y);
		this.x = FlxG.camera.width - 320;
		this.y = 40;
		loadGraphic("assets/images/distractor.png", true, 303, 179);
		flickering = FlxTween.tween(this, {alpha: 0}, 1, {type: FlxTween.PINGPONG});
	}	
	
	public function stopFlickering(){
		flickering.cancel();
		this.alpha = 1;
	}
	
	public function startFlickering() {
		flickering = FlxTween.tween(this, {alpha: 0}, 1, {type: FlxTween.PINGPONG});
	}
}