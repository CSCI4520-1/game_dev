package death;

import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxTimer;

class DeathState extends FlxState
{
	private var _background:FlxSprite;
	private var _sleepGrannie:FlxSprite;

	private var cutsceneTimer:FlxTimer = new FlxTimer();
	
	override public function create():Void
	{
		FlxG.camera.fade(FlxColor.BLACK, .33, true);
		_background = new FlxSprite();
		_background.loadGraphic("assets/images/death/death screen text.png");
		add(_background);
		
		_sleepGrannie = new FlxSprite();
		add(_sleepGrannie);
		_sleepGrannie.loadGraphic("assets/images/grannie_sleep_strip3.png", true, 512, 512);//grandma graphic 
		_sleepGrannie.setGraphicSize(375,375);
		_sleepGrannie.screenCenter();
		_sleepGrannie.animation.add("sleep", [1, 2, 3], 2, true);
		_sleepGrannie.animation.play("sleep");
		
		super.create();
		cutsceneTimer.start(7, next, 1);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	private function next(Timer:FlxTimer):Void
	{
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function() // longer fade to black when starting game
		{
			FlxG.switchState(new MenuState());
		});
	}
}
