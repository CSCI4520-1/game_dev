package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxColor;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.FlxObject;
/**
 * ...
 * @author ...
 */
class Wall extends FlxSprite
{
	private var _wallWidth:Int;
	private var _wallHeight:Int;
	
	
	public function new(?X:Int=0, ?Y:Int=0, ?width:Int=0, ?height:Int=0) 
	{
		super(X, Y);
		this.immovable = true;
		this._wallWidth = width; // store size info
		this._wallHeight = height;
		makeGraphic(width, height, FlxColor.RED);  // create the wall
	}
	
	public function getWallHeight() {
		return _wallHeight;
	}
	
	public function align(?direction:String)
	{
		if (direction == "left") 
		{
			this.x = 0;
			this.y = 0;
		}
		else if (direction == "right")
		{
			this.x = FlxG.camera.width - this._wallWidth;
			this.y = 0;
		}
		else // bottom is the default
		{
			this.x = 0;
			this.y = FlxG.camera.height - this._wallHeight;
			
		}
	}
	
	public function setVelocity(?V:Float=0)
	{
		this.velocity = new FlxPoint(0, 0);
		this.x = 0;
		this.y = FlxG.camera.height - this._wallHeight;
	}
}