package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.text.FlxText;
import lime.math.Vector2;
import flixel.math.FlxRandom;
import flixel.util.FlxTimer;
import flixel.system.FlxSound;

class AI extends FlxSprite{
	
	//Movement Variables
	var speed:Float = 0;
	public var _type:Int = 1; // Indicates speed/type of robot created
	var _rot:Float = 0;
	var _up:Bool = false;
	var _down:Bool = false;
	var _left:Bool = false;
	var _right:Bool = false;
	var _pause:Bool = false; // if True, robots should stop moving
	var _scapegoat:Bool = false; // whether the robot is the scapegoat
	var _bouncing:Bool = false; // whether the robot is bouncing off something
	var _animationPlaying:Bool = false; // So death animation overrides walk animation
	var rng:FlxRandom; 
	//Attack Variable 
	var attack:Bool = false;
	
	//Character Variables
	var playerX:Float;
	var playerY:Float;
	
	//var AiState._
	var player:Player;
	var scapegoat:AI;
	var _scapegoatTimer:FlxTimer = new FlxTimer();
	var _deathTimer:FlxTimer = new FlxTimer();
	var _frameSaverCount:Int = 0;
	var isDelayOver:Bool = false;
	private var _deathSound:FlxSound;
	var sprite:FlxSprite;

	public function new(p:Player, _type:Int){
		super();
		rng = new FlxRandom();

	if (_type == 1) {
			sprite = loadGraphic("assets/images/Enemy_1_whole_spritesheet_v1.png", true, 192, 192);
		} else if (_type == 2) {
			sprite = loadGraphic("assets/images/Enemy_2_whole_spritesheet_v2.png", true, 192, 192);
		} else { // _type = 3
			sprite = loadGraphic("assets/images/Enemy_3_whole_spritesheet_v2.png", true, 192, 192);
		}
		
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		animation.add("walk", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 30, true);
		animation.add("death", [30, 31, 32, 33, 34, 35], 30, false); 
		animation.add("frozen", [30, 31], 3, true ); 
		//this.updateHitbox();
		this.width = 120;
		this.height = 90;
		this.offset = new FlxPoint(20,75);

		drag.x = drag.y = 0;
		player = p;
		scapegoat = this;
		
		//Set starting location
		//this.x = -100;
		//this.y = -100;
		
		////////////////////
		//Border and Position variables
		var right:Float = 1500;
		var topLeft:Float = -300;
		var bottom:Float = 960;
		var xPos;
		var yPos;
		var stopLoopX:Bool = false;
		var stopLoopY:Bool = false;
		var rng:FlxRandom; 
		rng = new FlxRandom(); // random number generator
		
		//Set location
		do{
			if (stopLoopX == false){
				xPos = rng.float( -800, 1900);
				if (topLeft > xPos || xPos > right){
					this.x = xPos;
					stopLoopX = true;
				} 
				//else {
				//	stopLoopX = false;
				//}
			}
			if (stopLoopY == false) {
				yPos = rng.float( -300, 700);
				//if (topLeft > yPos ){
					this.y = yPos;
					stopLoopY = true;
				//} 
				//else {
				//	stopLoopY = false;
				//}
			}
		} while (stopLoopX == false || stopLoopY == false);	
		/////////////////////////
		_deathSound = FlxG.sound.load(AssetPaths.explosion__wav);
		haxe.Timer.delay((movementDelay.bind()), rng.int(0, 3000)); // 0-3s of random delay (3000)
		this.solid = false;

	}

	override public function update(elapsed:Float):Void{ 
		if (isDelayOver) {
		super.update(elapsed);
		if (_pause == false) {
			if (_frameSaverCount == 0) {
				movement();
				//_frameSaverCount = _frameSaverCount + 1;
			}
			else if (_frameSaverCount != 8){
				//_frameSaverCount = _frameSaverCount + 1;
			}
			else if (_frameSaverCount == 8) {
				//_frameSaverCount = 0;
			}
		}
		poll();
		
		//If touching player attack and die
		//FlxG.overlap(player, this, death);
		}
	}
	
	public function isAThreat():Bool {
		// logic to determine whether robot should harm human
		return !(_pause && _scapegoat);  // right now, paused robots (self destructing) should not harm granny while they are playing their death animation
	}
	
	public function pause():Void {
		_pause = true; // stops robots from moving
	}
	 
	public function unpause():Void {
		_pause = false;
	}
	function poll():Void{
		//if (FlxG.keys.anyPressed([T])){
			attack = true;
		//}
	}
	
	function deathAni(Timer:FlxTimer):Void {
		this.kill();
	}
	
	public function collisionDeath():Void{
		//called if player and robot are touching, robot dies - level handles health damage to player
				this.solid = false;
				animation.play("death");
				_animationPlaying = true;
				_deathTimer.start(0.2, deathAni, 1);
				_deathSound.play(true);
	}
	
	public function isScapegoat():Bool {
		return _scapegoat;
	}
	
	public function usbDeath(?weaponType:String):Void {
		// called if usb hits robot
		if (weaponType == "scapegoat") {
			sprite.setColorTransform(.45, 1, .45, .5, 0, 77, 0, 0);
			animation.play("frozen");
			sprite.scale = new FlxPoint(sprite.scale.x * 1.10, sprite.scale.y * 1.10);
			_pause = true;
			_scapegoat = true;
			this.immovable = true;
			this.solid = false;
			// freeze the enemy for 5 seconds then destroy
			_scapegoatTimer.start(5, timedDestroy, 1);
		}
		else {
			if (!_scapegoat) { // scapegoat will destroy himself
				
				this.solid = false;
				animation.play("death");
				_animationPlaying = true;
				_deathTimer.start(0.2, deathAni, 1);
				_deathSound.play(true);
				
			}
			
		}
	}
	
	public function targetScapegoat(?sg:AI) {
		scapegoat = sg;
	}
	
	
	public function timedDestroy(Timer:FlxTimer):Void{
				this.solid = false;
				animation.play("death");
				_animationPlaying = true;
				_deathTimer.start(0.2, deathAni, 1);
				_deathSound.play(true);
	}
	
	private function distance(?sg:AI){
		return Math.sqrt(((sg.x - this.x) * (sg.x - this.x)) + ((sg.y - this.y) *(sg.y - this.y)));
	}
	private function distanceToPlayer(?sg:Player){
		return Math.sqrt(((sg.x - this.x) * (sg.x - this.x)) + ((sg.y - this.y) *(sg.y - this.y)));
	}
		function nearPlayerBounce() {		
		velocity.set (velocity.x * 0, velocity.y * 0);
	}
	function bounce() {		
		velocity.set (velocity.x *0, velocity.y *0);
	}
	
	function movement():Void{
		//If robot is called to attack		
		if (attack == true){
			if ((scapegoat != this) && (scapegoat.alive)) { // scapegoat take priority, but don't target self! (the robot has scapegoat set to itself to avoid null checking
				//Find difference between player and self and move towards
				if (distance(scapegoat) < 70) {
					if (!_bouncing) {
						_bouncing = true;
						bounce();
					}
				}
				else { // advance towards scapegoat
					_bouncing = false;
					if (scapegoat.x < this.x){
						//_rot = 180;
						facing  = FlxObject.RIGHT;
					}
					else if (scapegoat.x > this.x){
						//_rot = 0;
						facing = FlxObject.LEFT;
					}
					
					//DEGREES
					_rot = Math.atan2(scapegoat.y - this.y, scapegoat.x - this.x)*180 / Math.PI;
					
					velocity.set (speed, 0);
					velocity.rotate(new FlxPoint(0, 0), _rot);
					if (!_animationPlaying) {
						animation.play("walk");
					}
				}
				
			}
			else { // focus on player
				//Find difference between player and self and move towards
				_bouncing = false;
				if (player.x < this.x){
					//_rot = 180;
					facing  = FlxObject.RIGHT;
				}
				else if (player.x > this.x){
					//_rot = 0;
					facing = FlxObject.LEFT;
				}
				
				//DEGREES
				_rot = Math.atan2(player.y - this.y, player.x - this.x)*180 / Math.PI;
				
				velocity.set (speed, 0);
				velocity.rotate(new FlxPoint(0,0), _rot);
				animation.play("walk");
					if (distanceToPlayer(player) < 20&&player.isInvincible()) {
						nearPlayerBounce();
					}
			}
			
			
		}
		else animation.stop();
	}
	public function movementDelay()
	{
		//delay
		if (_type == 1) {
			speed = 300;
		} else if (_type == 2) {
			speed = 400;
		} else { // _type = 3
			speed = 500;
		}
		this.solid = true;
		isDelayOver =  true;
	}
}