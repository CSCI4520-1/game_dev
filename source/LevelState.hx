package;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxColor;
import flixel.util.FlxCollision;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup.FlxTypedGroup;
import lime.tools.helpers.AIRHelper;
import flixel.math.FlxRandom;
import death.DeathState;
import flixel.system.FlxSound;
import flash.display.BlendMode;
import flixel.tweens.FlxTween;


class LevelState extends FlxState
{
	private var _leftWall:Wall;
	private var _rightWall:Wall;
	private var _floor:Wall;
	private var _background:FlxSprite;
	private var _background_effect:FlxSprite;
	private var _specialActivatedOverlay:FlxSprite;
	var _player:Player;
	var _usbs:FlxTypedGroup<USB>;
	var _robots:FlxTypedGroup<AI>;
	var _health:HealthBar;
	var _o:Bool;
	

	//////////////////////
	public var level:Int;
	public var waveNo:Float;
	//var speed:Int;
	public var levelTwo:Bool = false;
	public var levelThree:Bool = false;
	public var levelDone:Bool = false;
	var rng:FlxRandom; 

	//Set up sound variable
	private var _hurtSound:FlxSound;
	
	override public function create():Void
	{
		createLevel();
		
		rng = new FlxRandom(); // random number generator
		var stopLoop:Bool = false; 
		
		//Load Sounds
		_hurtSound = FlxG.sound.load(AssetPaths.grandmahit__wav);
		
		//create border and posisition variables
		var right:Float = 1280;
		var topLeft:Float = 0;
		var bottom:Float = 960;
		var xPos;
		var yPos;
		
		
		super.create();
		if (level == 1) {
			_background = new FlxSprite();
			_background.loadGraphic(AssetPaths.morning__png);
			_background.screenCenter();
			add(_background);
			_background_effect = new FlxSprite();
			_background_effect.loadGraphic(AssetPaths.morning_screen__png);
			_background_effect.screenCenter();
			_background_effect.blend = BlendMode.SCREEN;
			//add(_background_effect);
		} else if (level == 2) {
			_background = new FlxSprite();
			_background.loadGraphic(AssetPaths.noon__png);
			_background.screenCenter();
			add(_background);
			_background_effect = new FlxSprite();
			_background_effect.loadGraphic(AssetPaths.noon_screen__png);
			_background_effect.screenCenter();
			_background_effect.blend = BlendMode.SCREEN;
			//add(_background_effect);
		} else if (level == 3) {
			_background = new FlxSprite();
			_background.loadGraphic(AssetPaths.night__png);
			_background.screenCenter();
			add(_background);
			_background_effect = new FlxSprite();
			_background_effect.y = 50;
			_background_effect.loadGraphic(AssetPaths.night_multiply__png);
			_background_effect.screenCenter();
			_background_effect.blend = BlendMode.MULTIPLY;
			
		} else {
			_background = new FlxSprite();
			_background.loadGraphic(AssetPaths.start_background__png);
			_background.screenCenter();
			add(_background);
		}
		
		_usbs = new FlxTypedGroup<USB>();
		_player = new Player(350, FlxG.camera.height - 20 - 256, _usbs, this); // adds sprite at floor level
		add(_player);
		add(_usbs);
		
		// instantiate health system
		_health = new HealthBar(0, 0, 30, 30);
		
		
		//////////////////////////
		//create robots group
		_robots = new FlxTypedGroup<AI>();
		
		add(_robots);
		add(_background_effect);
		add(_health);
		
		////////////////////////////////////////////
	}

	override public function update(elapsed:Float):Void
	{
		if (_health.checkHealth() == 0) {
			gameOverLogic();
		}
		usbVisibleLogic();
		//scapegoatLogic();
		collisionLogic();
		super.update(elapsed);
		wave();
	}
	
	
	private function createLevel()
	{
		FlxG.camera.fade(FlxColor.BLACK, .33, true); // fade in
		//FlxG.camera.bgColor = new FlxColor(0xff0000ff); // bg color is blue just to differentiate between levels for now
		
		// create invisible walls on the left and right of the screen
		_leftWall = new Wall(0, 0, 5, FlxG.camera.height); // width, height
		_leftWall.align("left");
		_rightWall = new Wall(FlxG.camera.width - 5, 0, 5, FlxG.camera.height); // width, height
		_rightWall.align("right");
		_floor = new Wall(0, FlxG.camera.height - 40, FlxG.camera.width, 20);
		
		// add the walls
		add(_leftWall);
		add (_rightWall);
		add (_floor);
		
	}
	
	private function usbVisibleLogic(){
		for (usb in _usbs) {
			if (!usb.isOnScreen()) { // if the USB flies off screen, we don't want to track it
				_usbs.remove(usb); // remove from group
				usb.usbCollision(); // destroy
			}
		}
	}
	
	private function collisionLogic(){
		// Separates floor, walls and player (can't fall into floor)
		FlxG.collide(_floor, _player, onCollide_player_floor);  // player-floor collision (keeps them on ground)
		FlxG.collide(_rightWall, _player, onCollide_player_wall);
		FlxG.collide(_leftWall, _player, onCollide_player_wall);
		
		// robot / player collision
		for (robot in _robots){ // for each robot
			if (_health.checkHealth() != 0) { // don't bother colliding with the robots if Granny died
				FlxG.overlap(_player, robot, onCollide_player_robot); // check its collision with the player
			}
			else {
				robot.pause();
			}
		}
		if (_robots.countLiving() > 0) {
			if (_usbs.countLiving() > 0) {
				FlxG.collide(_usbs, _robots, onCollide_usb_robot);
			}
		}
		
		
	}
	
	function onCollide_player_floor(p:Player, w:Wall) {
		FlxObject.separate(p, w);
		if (_player.jumpStatus()) {
			_player.land();
		}
	}
	
	function onCollide_player_wall(p:Player, w:Wall) {
		FlxObject.separate(p, w);
	}
	
	function onCollide_player_robot(p:Player, r:AI){
		//FlxObject.separate(p, r);
		if (!(p.isInvincible())){ // if granny is not invincible
			if (r.isAThreat()) {
				_hurtSound.play(true);
				_health.deleteHealthIcon(); // lose one health unit
				p.setInvincibilityOnCollision(); // start the invincibility timer
				r.pause();
				r.collisionDeath(); // robot dies using death animation decided for collision death
				//r.usbDeath("normal")
			}	
		}
	}
	
	function onCollide_scapegoat_robot(scapegoat:AI, robot:AI){
		FlxObject.separate(scapegoat, robot);
	}
	
	function onOverlap_scapegoat_robot(scapegoat:AI, robot:AI){
		FlxObject.separate(scapegoat, robot);
		//FlxObject
	}
	
	function onCollide_usb_robot(u:USB, r:AI){
		/*// usb/robot collision
		for (usb in usbs) {
			for (robot in _robots) { // compare all usbs to all robots
				FlxG.collide(usb, robot, onCollide_usb_robot); // check its collision with the player
			}
		}*/
		if (u.getWeaponType() == "scapegoat"){
			r.pause(); // so it can do its little USB animation without moving
			r.usbDeath(u.getWeaponType()); // robot dies using death animation decided for collision death
			// all other robots should flock to it
			for (robot in _robots){ // for each robot
				if (!robot.isScapegoat()) { // if the robot isn't the scapegoat
					// send the scapegoat as the new target
					robot.targetScapegoat(r);
				}
			}
			
		}
		else { // normal attack
			if (!r.isScapegoat()) { // don't kill scapegoat with USBs
				r.pause(); // so it can do its little USB animation without moving
				r.usbDeath(u.getWeaponType()); // robot dies using death animation decided for collision death
			}
		}
		_usbs.remove(u); // remove the USB from the group
		u.usbCollision(); // destroy it TODO with animation
	}
		
	// if Granny dies
	private function gameOverLogic():Void {
		
		_player.playDeathAnimation(); // TODO
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function()
		{
			FlxG.switchState(new DeathState());
		});
		
		
	}
	
	///////////////
	public function makeRobot(speed:Int):Void{
		//var number = robots.length + 1;
		var _robot:AI;
		
			_robot = new AI(_player, speed);			
			_robots.add(_robot);
			
			if(speed == 3) {
				_robot.drag.x = _robot.drag.y = 10000;
				_robot._type = 3;
			} else if(speed == 2) {
				_robot.drag.x = _robot.drag.y = 25000;
				_robot._type = 2;
			} else if(speed == 1){
				_robot.drag.x = _robot.drag.y = 50000;
				_robot._type = 1;
			}
		
	}
	
	
	
	public function wave():Void{
		//Transition between waves in levels
		if (level == 1){
			/////////////////////Level One
			if (waveNo == 1){
				//Create 3 robots
				for (i in 0...3){
					makeRobot(1);
				}
				waveNo = 1.5;
			} else if (waveNo == 1.5){
				if (_robots.countLiving() == 0){
					waveNo = 2;
				}
			} else if (waveNo == 2.0){
				for (i in 0...5){
					makeRobot(1);
				}
				waveNo = 2.5;
			} else if (waveNo == 2.5){
				if (_robots.countLiving() == 0){
					waveNo = 3;
				}
			} else if (waveNo == 3.0){
				for (i in 0...7){
					makeRobot(1);
				}
				waveNo = 3.5;
			} else if (waveNo == 3.5){
				if (_robots.countLiving() == 0){
					//NEXT LEVEL
					levelTwo = true;
				}
			}
			
		} else if (level == 2){
			/////////////////////Level Two
			if (waveNo == 1){
				//Create 3 robots
				for (i in 0...4){
					makeRobot(1);
				}
				makeRobot(2);
				waveNo = 1.5;
			} else if (waveNo == 1.5){
				if (_robots.countLiving() == 0){
					waveNo = 2;
				}
			} else if (waveNo == 2.0){
				for (i in 0...5){
					makeRobot(1);
				}
				for (i in 0...3){
					makeRobot(2);
				}
				waveNo = 2.5;
			} else if (waveNo == 2.5){
				if (_robots.countLiving() == 0){
					waveNo = 3;
				}
			}else if (waveNo == 3.0){
				for (i in 0...6){
					makeRobot(1);
				}
				for (i in 0...4){
					makeRobot(2);
				}
				waveNo = 3.5;
			} else if (waveNo == 3.5){
				if (_robots.countLiving() == 0){
					waveNo = 4;
				}
			} else if (waveNo == 4.0){
				for (i in 0...6){
					makeRobot(1);
				}
				for (i in 0...5){
					makeRobot(2);
				}
				waveNo = 4.5;
			} else if (waveNo == 4.5){
				if (_robots.countLiving() == 0){
					waveNo = 5;
				}
			} else if (waveNo == 5.0){
				for (i in 0...6){
					makeRobot(1);
					makeRobot(2);
				}
				waveNo = 5.5;
			} else if (waveNo == 5.5){
				if (_robots.countLiving() == 0){
					waveNo = 6;
				}
			} else if (waveNo == 6.0){
				for (i in 0...8){
					makeRobot(1);
					makeRobot(2);
				}
				waveNo = 6.5;
			} else if (waveNo == 6.5){
				if (_robots.countLiving() == 0){
					//NEXT LEVEL
					levelThree = true;
				}
			}
			
		} else if (level == 3){
			/////////////////////Level Three
			if (waveNo == 1){
				//Create 3 robots
				for (i in 0...2){
					makeRobot(1);
					makeRobot(2);
				}
				makeRobot(3);
				waveNo = 1.5;
			} else if (waveNo == 1.5){
				if (_robots.countLiving() == 0){
					waveNo = 2;
				}
			} else if (waveNo == 2.0){
				//for (robot in _robots) {
				//	robot.destroy();
				//}
				//for (usb in _usbs) {
				//	if (!usb.alive) {
				//		_usbs.remove(usb);
				//		usb.destroy();
				//	}
				//}
				_robots.clear();
				for (i in 0...2){
					makeRobot(1);
					makeRobot(2);
					makeRobot(3);
				}
				waveNo = 2.5;
			} else if (waveNo == 2.5){
				if (_robots.countLiving() == 0){
					waveNo = 3;
				}
			}else if (waveNo == 3.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...3){
					makeRobot(1);
					makeRobot(2);
				}
				for (i in 0...3){
					makeRobot(3);
				}
				waveNo = 3.5;
			} else if (waveNo == 3.5){
				if (_robots.countLiving() == 0){
					waveNo = 4;
				}
			} else if (waveNo == 4.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...5){
					makeRobot(1);
					makeRobot(2);
				}
				for (i in 0...3){
					makeRobot(3);
				}
				waveNo = 4.5;
			} else if (waveNo == 4.5){
				if (_robots.countLiving() == 0){
					waveNo = 5;
				}
			} else if (waveNo == 5.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...2){
					makeRobot(1);
					makeRobot(2);
				}
				for (i in 0...5){
					makeRobot(3);
				}
				waveNo = 5.5;
			} else if (waveNo == 5.5){
				if (_robots.countLiving() == 0){
					waveNo = 6;
				}
			} else if (waveNo == 6.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...1){
					makeRobot(1);
					makeRobot(2);
				}
				for (i in 0...7){
					makeRobot(3);
				}
				waveNo = 6.5;
			} else if (waveNo == 6.5){
				if (_robots.countLiving() == 0){
					waveNo = 7;
				}
			} else if (waveNo == 7.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...14){
					makeRobot(3);
				}
				waveNo = 7.5;
			} else if (waveNo == 7.5){
				if (_robots.countLiving() == 0){
					waveNo = 8;
				}
			} else if (waveNo == 8.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...4){
					makeRobot(1);
					makeRobot(2);
					makeRobot(3);
				}
				waveNo = 8.5;
			} else if (waveNo == 8.5){
				if (_robots.countLiving() == 0){
					waveNo = 9;
				}
			} else if (waveNo == 9.0){
				for (robot in _robots) {
					robot.destroy();
				}
				for (usb in _usbs) {
					if (!usb.alive) {
						_usbs.remove(usb);
						usb.destroy();
					}
				}
				_robots.clear();
				for (i in 0...5){
					makeRobot(1);
					makeRobot(2);
					makeRobot(3);
					makeRobot(3);
				}
				waveNo = 9.5;
			} else if (waveNo == 9.5){
				if (_robots.countLiving() == 0){
					//NEXT LEVEL
					levelDone = true;
				}
			} 
		}
		
		
	}
	
	
	///////////////
	
}
