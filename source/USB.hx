package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.FlxObject;
import flixel.FlxG;
import flixel.math.FlxPoint;

/**
 * ...
 * @author ...
 */
class USB extends FlxSprite 
{
	var weapon_type:String;
	var usbSpeed = 1.25;
	public function new(?X:Float = 0, ?Y:Float = 0, ?direction:String, ?weaponType:String) 
	{
		super(X, Y);
		if (weaponType == "scapegoat"){
			loadGraphic("assets/images/special_flashdrive_spritesheet.png", true, 128, 128);

		}
		else 
		{
			 loadGraphic("assets/images/normal_flashdrive_spritesheet.png", true, 128, 128);
			 this.setGraphicSize(90, 90);

		}
		setFacingFlip(FlxObject.LEFT, true, true);
		setFacingFlip(FlxObject.RIGHT, true, true);
		animation.add("fly", [0, 1, 2, 3, 4, 5, 6], 15);
		this.updateHitbox();
		directionLogic(direction);
		weapon_type = weaponType;
		usbSpeed = 1.5;
	}
	
	override public function update(elapsed:Float):Void{
		super.update(elapsed);
	}	
	
	public function directionLogic(direction:String):Void {
		animation.play("fly");
		if (direction == "right") {
			this.velocity.x = 1000*usbSpeed;
			this.facing = FlxObject.RIGHT;
		}
		else if (direction == "up-right") {
			this.velocity.x = 525*usbSpeed;
			this.velocity.y = -525*usbSpeed;
			this.angle = -45;
			this.facing = FlxObject.RIGHT;
		}
		else if (direction == "up") {
			this.velocity.y = -1000*usbSpeed;
			this.angle = 90;
		}
		else if (direction == "up-left") {
			this.facing = FlxObject.LEFT;
			this.velocity.x = -525*usbSpeed;
			this.velocity.y = -525*usbSpeed;
			this.angle = -135;
		}
		else if (direction == "down-left") {
			this.facing = FlxObject.LEFT;
			this.velocity.x = -525*usbSpeed;
			this.velocity.y = 525*usbSpeed;
			this.angle = 135;
		}
		else if (direction == "down-right") {
			this.velocity.x = 525*usbSpeed;
			this.velocity.y = 525*usbSpeed;
			this.angle = 45;
			this.facing = FlxObject.RIGHT;
		}
		else if (direction == "down") {
			this.velocity.y = 1000*usbSpeed;
			this.angle = 270;
		}
		else { // must be right
			this.facing = FlxObject.LEFT;
			this.velocity.x = -1000*usbSpeed;
			this.angle = 180;
		}
	}
	
	public function getWeaponType():String {
		return weapon_type;
	}
	
	public function usbCollision():Void {
		// called when this USB hits a robot
		// TODO disintegration animation?
		animation.stop();
		this.kill();
	}
	
}