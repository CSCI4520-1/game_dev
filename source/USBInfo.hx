package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.group.FlxSpriteGroup;
import flixel.FlxG;
import flixel.text.FlxText;
import flixel.util.FlxTimer;
/**
 * ...
 * @author ...
 */
class USBInfo extends FlxSpriteGroup 
{
	var _currentWeaponGraphic:USBInfoGraphic;
	var _currentWeaponText:FlxText;
	var _mainWeaponGraphic:USBInfoGraphic;
	var _specialTimer:FlxTimer = new FlxTimer();
	var readied:Bool = false;
	var allowed:Bool = false;
	var _player:Player;
	
	
	public function new(?X:Float=0, ?Y:Float=0, player:Player) 
	{
		super(X, Y);
		_currentWeaponGraphic = new USBInfoGraphic(0.0, 0.0);
		add(_currentWeaponGraphic);
		_player = player;
		//_currentWeaponText = new FlxText(FlxG.camera.width - 235, 140, 110, "D157R4C7", 16);
		//add(_currentWeaponText);
	}
	
	public function disappear() {
		this.visible = false;
	}
	
	// changes display to look different when special is prepared
	public function readySpecial() {
		if (!readied&&_currentWeaponGraphic.visible){
			_currentWeaponGraphic.stopFlickering();
			readied = true;
			
		}
		
	}
	
	// changes display back to normal
	public function unreadySpecial() {
		if (readied) {
			_currentWeaponGraphic.startFlickering();
			readied = false;
		}
		
	}
	
	// 
	public function useSpecial() {
		disappear();
		// set a timer for it to reappear
		_specialTimer.start(15, appear, 0); // start the timer that controls how fast USBs are shot
		allowed = false;
		
	}
	
	public function appear(Timer:FlxTimer) {
		this.visible = true;
		allowed = true;
		_player.enableSpecialAvailable();
	}
	
	public function specialIsReady():Bool {
		return this.visible;
	}
	
	public function specialAllowed():Bool {
		return allowed;
	}
	
}