package;

import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import flixel.math.FlxPoint;
import intro_states.IntroState1;

class MenuState extends FlxState
{
	private var _gameTitle:FlxText;
	private var _startGame:FlxButton;
	private var _background:FlxSprite;
	private var _pieGrannie:FlxSprite;

	
	override public function create():Void
	{
		/*_gameTitle = new FlxText(20, 0, 0, "Kill Command", 24);
		_gameTitle.alignment = CENTER;
		_gameTitle.x = (FlxG.width / 2) - (_gameTitle.width/2);
		_gameTitle.y = FlxG.height -350; 
		add(_gameTitle);*/
		_background = new FlxSprite();
		_background.loadGraphic("assets/images/backgrounds/start_background.png");
		add(_background);
		_startGame = new FlxButton(640, 850, "", clickPlay); // start game button
		_startGame.loadGraphic("assets/images/menu start button.png");
		_startGame.screenCenter();
		_startGame.setPosition(_startGame.getPosition().x, 800);
		add(_startGame); // add it
		
		_pieGrannie = new FlxSprite();
		add(_pieGrannie);
		_pieGrannie.loadGraphic("assets/images/grannie_pie_strip12.png", true, 512, 512);//grandma graphic 
		_pieGrannie.setGraphicSize(375,375);
		_pieGrannie.screenCenter();
		_pieGrannie.animation.add("pie", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 10, true);
		_pieGrannie.animation.play("pie");
		//_pieGrannie.
		_pieGrannie.screenCenter();
		_pieGrannie.setPosition(_pieGrannie.getPosition().x, 350);
		_pieGrannie.angle = 4;
		super.create();
		

		//Play Music
		FlxG.sound.playMusic(AssetPaths.gameMusic__wav, 0.5, true);
		
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
	}
	
	private function clickPlay():Void
	{
		FlxG.camera.fade(FlxColor.BLACK,2, false, function() // longer fade to black when starting game
		{
			FlxG.switchState(new IntroState1());
		});
	}
}
