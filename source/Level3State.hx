package;

import flixel.FlxState;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxSprite;
import win_game_cutscenes.WinGameState1;

class Level3State extends LevelState
{
	
	override public function create():Void
	{
		level = 3;
		super.create();
		waveNo = 1;
		_player.setSpecialAvailable(this);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		if (levelDone == true){
			winGame();
		}
	}
	
	private function winGame():Void
	{
		FlxG.camera.fade(FlxColor.BLACK,0.33, false, function()
		{
			FlxG.switchState(new WinGameState1());
		});
	}
}
