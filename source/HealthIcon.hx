package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class HealthIcon extends FlxSprite
{

	public function new(?X:Float=0, ?Y:Float=0,  ?width:Int=0, ?height:Int=0) 
	{
		super(X, Y);
		makeGraphic(width, height, FlxColor.RED);  // create the individual icon
	}
	
}