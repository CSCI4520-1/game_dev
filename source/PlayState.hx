package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxG;
import flixel.util.FlxColor;
import source.Player;

class PlayState extends FlxState
{
	var _player:Player;
	
	override public function create():Void
	{
		_player = new Player(150,150);
		add(_player);
		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
}

